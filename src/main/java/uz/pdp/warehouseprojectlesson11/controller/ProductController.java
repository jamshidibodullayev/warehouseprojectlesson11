package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.Product;
import uz.pdp.warehouseprojectlesson11.entity.User;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.payload.ProductDto;
import uz.pdp.warehouseprojectlesson11.repository.ProductRepo;
import uz.pdp.warehouseprojectlesson11.service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ProductController {


    @Autowired
    ProductService productService;

    @Autowired
    ProductRepo productRepo;

    @GetMapping()
    public List<Product> get() {
        return productRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return productService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addProduct(@RequestBody ProductDto dto) {
        return productService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return productService.delete(id);
    }


}
