package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.Currency;
import uz.pdp.warehouseprojectlesson11.entity.Warehouse;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.CurrencyRepo;
import uz.pdp.warehouseprojectlesson11.repository.WarehouseRepo;
import uz.pdp.warehouseprojectlesson11.service.CurrencyService;
import uz.pdp.warehouseprojectlesson11.service.WarehouseService;

import java.util.List;

@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    WarehouseRepo warehouseRepo;


    @GetMapping("/all")
    public List<Warehouse> get() {
        return warehouseRepo.findAll();
    }


    @PostMapping("/addOrEdit")
    public ApiResponse addOrEdit(@RequestBody Warehouse warehouse) {
        return warehouseService.addOrEdit(warehouse);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return warehouseService.delete(id);
    }

}
