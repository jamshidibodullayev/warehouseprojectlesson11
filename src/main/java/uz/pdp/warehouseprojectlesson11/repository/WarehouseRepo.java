package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Warehouse;

public interface WarehouseRepo extends JpaRepository<Warehouse, Long> {

}
