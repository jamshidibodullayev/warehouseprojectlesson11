package uz.pdp.warehouseprojectlesson11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Warehouseprojectlesson11Application {

    public static void main(String[] args) {
        SpringApplication.run(Warehouseprojectlesson11Application.class, args);
    }

}
