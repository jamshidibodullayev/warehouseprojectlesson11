package uz.pdp.warehouseprojectlesson11.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.warehouseprojectlesson11.entity.Currency;
import uz.pdp.warehouseprojectlesson11.entity.Warehouse;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.WarehouseRepo;

import java.util.Optional;

@Service
public class WarehouseService {

    @Autowired
    WarehouseRepo warehouseRepo;

    public ApiResponse addOrEdit(Warehouse warehouse) {
        try {
            Warehouse warehouse1 = new Warehouse();
            if (warehouse.getId() != null) {
                warehouse1 = warehouseRepo.getById(warehouse.getId());
            }
            warehouse1.setName(warehouse.getName());
            warehouseRepo.save(warehouse1);
            return new ApiResponse(true, warehouse.getId() != null ? "Edited!" : "Saved");
        } catch (Exception e) {
            return new ApiResponse(false, "Error!");
        }
    }


    public ApiResponse delete(Long id) {
        Optional<Warehouse> optionalWarehouse = warehouseRepo.findById(id);
        if (!optionalWarehouse.isPresent()) {
            return new ApiResponse(false, "Error on Deleting!");
        }
        warehouseRepo.delete(optionalWarehouse.get());
        return new ApiResponse(true, "Deleted!");
    }
}
